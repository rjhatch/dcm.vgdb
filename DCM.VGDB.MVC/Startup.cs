﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DCM.VGDB.MVC.Startup))]
namespace DCM.VGDB.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
