﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DCM.VGDB.MVC.Controllers
{
    public class SystemsController : Controller
    {
        DCM.VGDB.BLL.Provider.SystemProvider p = new BLL.Provider.SystemProvider();

        // GET: Console
        public ActionResult Index(string id)
        {
            return View(p.GetByTitle(id));
        }

        // GET: Console/Details/5
        public ActionResult Details(string system_name)
        {
            return View();
        }

        public ActionResult NavigationList()
        {
            return PartialView("_SystemNavigation", p.GetList());
        }

        // GET: Console/Create
        [Authorize(Roles="Administrators")]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Console/Create
        [HttpPost]
        [Authorize(Roles = "Administrators")]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Console/Edit/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Console/Edit/5
        [HttpPost]
        [Authorize(Roles = "Administrators")]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Console/Delete/5
        [Authorize(Roles = "Administrators")]
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Console/Delete/5
        [HttpPost]
        [Authorize(Roles = "Administrators")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
