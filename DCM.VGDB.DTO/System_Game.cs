﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.DTO
{
    public class System_Game
    {
        public int SystemID { get; set; }
        public int GameID { get; set; }

        public System_Game()
        {
            SystemID = 0;
            GameID = 0;
        }
    }
}
