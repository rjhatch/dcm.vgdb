﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.DTO
{
    public class SystemDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public List<GameDTO> Games { get; set; }

        public SystemDTO()
        {
            ID = 0;
            Title = string.Empty;
            Description = string.Empty;

            Games = new List<GameDTO>();
        }

        public SystemDTO(DAL.GameSystem game_system)
        {
            ID = game_system.ID;
            Title = game_system.Title;
            Description = game_system.Description;

            Games = new List<GameDTO>();
        }
    }
}
