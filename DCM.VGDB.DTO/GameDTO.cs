﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.DTO
{
    public class GameDTO
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }

        public GameDTO()
        {
            ID = 0;
            Title = string.Empty;
            Description = string.Empty;
        }

        public GameDTO(DAL.Game game)
        {
            ID = game.ID;
            Title = game.Title;
            Description = game.Description;
        }
    }
}
