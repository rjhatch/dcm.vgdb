INSERT INTO [dbo].[Game]
           ([GameTitle], [GameDescription])
     VALUES
           ('Super Mario Brothers', 'Super Mario Brothers Description'),
           ('Super Mario Brothers 2', 'Super Mario Brothers 2 Description'),
           ('Super Mario Brothers 3', 'Super Mario Brothers 3 Description'),
           ('Contra', 'Contra Description'),
           ('Tetris', 'Tetris Description'),
           ('Kirby', 'Kirby Description')
GO

INSERT INTO [dbo].[System]
           ([SystemTitle], [SystemDescription])
     VALUES
           ('NES', 'Nintendo Entertainment System'),
           ('SNES', 'Super Nintendo Entertainment System'),
           ('SMS', 'Sega Master System'),
           ('Genesis', 'Sega Genesis')
GO

INSERT INTO System_Game
	SELECT	SystemID,
			GameID
	FROM	[Game], [System]
	WHERE	[System].[SystemTitle] = 'NES'
GO