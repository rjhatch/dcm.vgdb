﻿using System;
using System.Configuration;

namespace DCM.VGDB.Common
{
    public static class ConfigurationHelper
    {
        /// <summary>
        /// This will get the DCM.VGDB ConnectionString from the Configuration.ConnectionStrings section
        /// </summary>
        public static string ConnectionString
        {
            get
            {
                try { return ConfigurationManager.ConnectionStrings["DCM.VGDB"].ConnectionString; }
                catch { throw new NotImplementedException("DCM.VGDB ConnectionString doesn't exist in the configuration file for this application."); }
            }
        }
    }
}
