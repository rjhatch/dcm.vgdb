﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DCM.VGDB.Common;

namespace DCM.VGDB.BLL.Provider
{
    public class GameProvider : ProviderBase
    {

        public List<DTO.GameDTO> GetList(string system_name)
        {
            List<DTO.GameDTO> games = new List<DTO.GameDTO>();
            _db.GameSystems.First(s => s.Title.Equals(system_name, StringComparison.OrdinalIgnoreCase))
                .Games.OrderBy(g => g.Title).ToList()
                .ForEach(g => games.Add(new DTO.GameDTO(g)));
            return games;
        }
        public List<DTO.GameDTO> GetRandomList(string system_name, int count)
        {
            List<DTO.GameDTO> games = GetList(system_name);
            games.Shuffle();
            if (games.Count > count)
                games.RemoveRange(count, games.Count - count);
            return games;
        }

        public DTO.GameDTO GetByID(int id)
        {
            return new DTO.GameDTO(_db.Games.FirstOrDefault(s => s.ID == id));
        }
    }
}
