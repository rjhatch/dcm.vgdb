﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.BLL.Provider
{
    public class SystemProvider : ProviderBase, Interfaces.ISystem
    {

        public List<DTO.SystemDTO> GetList()
        {
            List<DTO.SystemDTO> systems = new List<DTO.SystemDTO>();
            _db.GameSystems.OrderBy(s => s.Title).ToList().ForEach(s => systems.Add(new DTO.SystemDTO(s)));
            return systems;
        }

        ////public T Create<T>(T obj)
        ////{
        ////    throw new NotImplementedException();
        ////}

        public DTO.SystemDTO GetByID(int id)
        {
            return new DTO.SystemDTO(_db.GameSystems.FirstOrDefault(s => s.ID == id));
        }

        public DTO.SystemDTO GetByTitle(string title)
        {
            return new DTO.SystemDTO(_db.GameSystems.FirstOrDefault(s => s.Title.Equals(title, StringComparison.OrdinalIgnoreCase)));
        }

        ////public T Update<T>(T obj)
        ////{
        ////    throw new NotImplementedException();
        ////}

        ////public void Delete<T>(int id)
        ////{
        ////    throw new NotImplementedException();
        ////}
    }
}
