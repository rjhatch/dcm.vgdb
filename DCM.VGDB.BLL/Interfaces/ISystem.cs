﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.BLL.Interfaces
{
    public interface ISystem
    {
        List<DTO.SystemDTO> GetList();

        DTO.SystemDTO GetByID(int id);

        DTO.SystemDTO GetByTitle(string title);

        //SystemDTO
    }
}
