﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.BLL.Interfaces
{
    public interface ICRUD
    {
        T Create<T>(T obj);
        
        T Read<T>(int id);
        
        T Update<T>(T obj);

        void Delete<T>(int id);
    }
}
