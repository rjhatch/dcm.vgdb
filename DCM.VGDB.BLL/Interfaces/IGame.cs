﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCM.VGDB.BLL.Interfaces
{
    public interface IGame
    {
        List<DTO.GameDTO> GetList();

        DTO.GameDTO GetByID(int id);

        DTO.GameDTO GetByTitle(string title);

        //SystemDTO
    }
}
